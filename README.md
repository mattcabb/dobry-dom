# Założenia dotyczące budowy przyszłego domu
Kilka przemyśleń z perspektywy czasu po wybudowaniu pierwszego domu.
Wpisy mogą się powtarzać pod innymi kategoriami, jeżeli jest to uzasadnione.

## Główne założenia:
* Jeden poziom.
* Płaski dach.
* Bez piwnic, bez poddasza, bez schodów.
* Szafy i inne systemy przechowywania we wnękach.
* Automatyka, ale "niewidzialna" (bez ekranów sterujących i paneli kontrolnych na ścianach).

## Pomieszczenia

### Salon / Jadalnia
* Paski LED za sufitem, za telewizorem oparte o WS2812 lub tradycyjną taśmę RGB.
* Wyprowadzenie zasilania i Ethernet na wysokości TV.
* Stół jadalniany na centralnej nodze.

### Kuchnia
* Wyższy sufit w kuchni, aby ograniczyć wydostawanie się zapachów z kuchni.
* Przewody z sygnałem audio rozprowadzone do: kuchnia, łazienka, taras/ogród.
* Sygnał wideo udostępniony do kuchni, aby umożliwić montaż TV pod sufitem.
* Szkło nad blatem (najłatwiejsze czyszczenie) z ciemnym zadrukiem od spodu.
* Osobny obwód el. dla lodówki.
* Indukcyjna płyta grzewcza (czyste naczynia, mniejsza bezwładność niż płyta żarowa).
* Piekarnik na wysokości blatu (nie trzeba się schylać po gorące).
* Piekarnik i mikrofalówka w jednym (np. Samsung NQ50H5537KB).
* Wyprowadzenie 230V pod kuchenną wyspę (do użytku AGD oraz ładowania RTV).
* Szybko ustalić gdzie będzie płyta grzewcza, aby doprowadzić zasilanie i przewody powietrzne dla wyciągu.
* Bez wiszących szafek lub wiszące szafki dociągnięte do sufitu, żeby nie zbierało kurzu i gratów.
* Bez szafek rogowych (drogie i niewygodne karuzele na naczynia...).
* Meble IKEA (długa gwarancja i dostępność elementów zamiennych).
* Dużo obwodów el. w kuchni (dużo urządzeń o wysokim poborze).
* Zamiast cargo rozważyć spiżarnię z drzwiami z zabudowy kuchennej.

### Łazienka/Pralnia
* Przewody z sygnałem audio rozprowadzone do: kuchnia, łazienka, taras/ogród.
* Głośniki sufitowe, zasilanie razem ze światłem.
* Paski LED pod lustrem, za sufitem... oparte o WS2812 lub nowsze.
* Pralkosuszarka.
* Osobny obwód el. dla pieca grzewczego i rozdzielacza wody.
* Grzejnik na ręczniki z czasowym wyłącznikiem.
* Wentylacja mechaniczna na czujniku wilgotności i temperatury w łazience.
* Opuszczany sufit w łazience ze szczeliną do wyciągania powietrza zamiast kratki wentylacyjnej.
* W umywalce ma się mieścić siedzące roczne dziecko.
* Zamiast tradycyjnej kratki wywietrznika, szczelina w podwieszanym suficie.

### Sypialnie
* Paski LED za sufitem, pod ramą łóżka oparte o WS2812.
* Wyprowadzony Ethernet i zasilanie na wysokości ekranu na przeciwko łóżka.
* Sterowanie roletami przy włącznikach światła + możliwość sterowania z automatyki domowej.

### Ogród, taras, ogrodzenie
* Wyprowadzone 230V przy budynku i na końcu działki.
* Przewody z sygnałem audio rozprowadzone do: kuchnia, łazienka, taras/ogród.
* Deszczówka napełniająca zbiornik, przelew odprowadzany do kanalizacji.
* Obieg wody z kolektorem słonecznym w formie zadaszenia tarasu.
* Lądowisko dla drona z grafiką SpaceX jako jedna z płytek chodnika.
* Okablowanie gotowe do założenia kamer na zewnątrz.
* Furtka ogrodowa z czujnikiem otwarcia i/lub elektrozamkniem.
* System nawadniania ogrodu z automatyką i ręcznym wyzwalaniem.
* Zadaszenie tarasu w formie żagla.
* Fotowoltaika w formie zadaszenia tarasu.

### Podjazd, garaż/wiata i komórka
* Przygotowanie do szybkiego ładowania pojazdów elektrycznych w garażu i na podjeździe.
* Garaż z warsztatem/pracownią.
* Okablowanie gotowe do założenia kamer na zewnątrz.
* Oświetlenie podjazdu.
* Zewnętrzna czujka ruchu zmieniająca oświetlenie na podjeździe.

### Spiżarnia przy kuchni
* Nieopłaclane szafy CARGO (wysoki koszt/niska jakość/słaba pojemność).
* Zamiast cargo rozważyć spiżarnię z drzwiami z zabudowy kuchennej.
* Głębokie szuflady zamiast niektórych szafek.

## Kategorie

### Okablowanie i Instalacja Elektryczna
* Przewody z sygnałem audio rozprowadzone do: kuchnia, łazienka, taras/ogród.
* Przygotowanie do szybkiego ładowania pojazdów elektrycznych w garażu i na podjeździe.
* Wyprowadzony Ethernet i zasilanie na wysokości ekranu na przeciwko łóżka.
* Wyprowadzenie zasilania i Ethernet na wysokości TV.
* Dzwonek do drzwi zamontowany wewnątrz szafy, żeby nie szpecić ściany.
* Furtka ogrodowa z czujnikiem otwarcia i/lub elektrozamkniem.
* 2x 8 żył na dach do centrali pogodowej.
* Osobny obwód el. dla lodówki.
* Dużo obwodów el. w kuchni (dużo urządzeń o wysokim poborze).
* Osobny obwód el. dla pieca grzewczego.
* Osobny odwód el. dla alarmu.
* Osobny obwód el. dla centrali automatyki.
* Wyprowadzone 230V przy budynku i na końcu działki.
* Od razu wprowadzić światłowód do budynku.
* Gniazdko elektryczne umiejscowione tak, aby przewód odkurzacza sięgał na cały poziom.
* Wyprowadzenie 230V pod kuchenną wyspę (do użytku AGD oraz ładowania RTV).
* Szybko ustalić gdzie będzie płyta grzewcza, aby doprowadzić zasilanie i przewody powietrzne dla wyciągu.

### Woda, prąd, gaz
* Piec gazowy + kolektor ciepła.
* Deszczówka napełniająca zbiornik, przelew odprowadzany do kanalizacji deszczowej.
* Fotowoltaika w formie zadaszenia tarasu.
* Obieg wody z kolektorem słonecznym w formie zadaszenia tarasu.
* Osobny obwód el. dla pieca grzewczego.

### Wentylacja i ogrzewanie
* Ogrzewanie podłogowe na całości.
* Nie montować termostatów na ścianach. Przy jednym poziomie wystarczy regulacja rozdzielaczu wody.
* Podłączenie pieca do centrali pogodowej na dachu (krzywa grzewcza).
* Klimtyzator wewnętrzny zabudowany nad sufitem, widoczny tylko wylot (ciszej i estetyczniej).
* Wentylacja mechaniczna na czujniku wilgotności i temperatury w łazience.
* Opuszczany sufit w łazience ze szczeliną do wyciągania powietrza zamiast kratki wentylacyjnej.
* Osobny obwód el. dla pieca grzewczego i rozdzielacza wody.
* Czujniki jakości powietrza.
* Szybko ustalić gdzie będzie płyta grzewcza, aby doprowadzić zasilanie i przewody powietrzne dla wyciągu.

### Oświetlenie
* Paski LED oparte o WS2812 lub nowsze.
* W niektórych pomieszczeniach cokoły z korytkiem na LED.
* Czujki ruchu sterujące oświetleniem asekuracyjnym w trybie nocnym.
* Zewnętrzna czujka ruchu zmieniająca oświetlenie na podjeździe.
* Włączniki światła w zasięgu dziecka lub czujniki ruchu oświetlenia.

### Multimedia
* Przewody z sygnałem audio rozprowadzone do: kuchnia, łazienka, taras/ogród.
* Sygnał wideo udostępniony do kuchni, aby umożliwić montaż TV pod sufitem.
* Wyprowadzony Ethernet i zasilanie na wysokości ekranu na przeciwko łóżka.
* Wyprowadzenie zasilania i Ethernet na wysokości TV.
* Zamiast głośników wolnostojących zabudowane w meblach za tkaniną głośnikową.

### Automatyka, instalacja alarmowa, monitoring
* Sterowanie roletami przy włącznikach światła + możliwość sterowania z automatyki domowej.
* Czujki ruchu sterujące oświetleniem asekuracyjnym w trybie nocnym.
* RaspberryPi 3B+ jako centrala automatyki, HomeBridge do kontroli przez iOS HomeKit.
* Okablowanie gotowe do założenia kamer na zewnątrz.
* Zewnętrzna czujka ruchu zmieniająca oświetlenie na podjeździe.
* Furtka ogrodowa z czujnikiem otwarcia i/lub elektrozamkniem.
* Kontaktrony pod każdym skrzydłem okiennym na każdych drzwiach.
* System nawadniania ogrodu z automatyką i ręcznym wyzwalaniem.
* Osobny odwód el. dla alarmu.
* Osobny obwód el. dla centrali automatyki.
* Grzejnik na ręczniki z czasowym wyłącznikiem.
* Miejsca trudnodostępne kontrolowane przez moduł ESP8266.
* Termistor pod wanną.

### Drzwi i okna
* Sterowanie roletami przy włącznikach światła + możliwość sterowania z automatyki domowej.
* Kontaktrony pod każdym skrzydłem okiennym na każdych drzwiach.
* Klamki okien z zamkami.

### Podłogi
* Ogrzewanie podłogowe.
* Nie kłaść podłogówki pod zmywarką, żeby nie śmierdziała przed uruchomieniem.
* Cokoły z elastycznego tworzywa lub z gumowym rantem.
* W niektórych pomieszczeniach cokoły z korytkiem na LED.
* Łączenia podłóg z korka dylatacyjnego.

### Sufity
* Wyższy sufit w kuchni, aby ograniczyć wydostawanie się zapachów z kuchni.
* Głośniki sufitowe w łazience.
* Zamiast tradycyjnej kratki wywietrznika w łazience, szczelina w podwieszanym suficie.

### Roślinność
* Deszczówka napełniająca zbiornik, przelew odprowadzany do kanalizacji deszczowej.
* Pompa wody w zbiorniku podłączona do systemu nawadniania + wąż ogrodowy.
* System nawadniania ogrodu z automatyką i ręcznym wyzwalaniem.

### Dekoracja wnętrza
* Imitacje plakatów filmowych ze zdjęć rodzinnych dla ważnych momentów.

## Tematy do przemyślenia
* Obwód zasilania awaryjnego 12V do oświetlenia "ewakuacyjnego"?
* Małe panele słoneczne w formie dachówki (zob. Solar City > Solar Roof)?
